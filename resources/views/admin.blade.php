<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <style>
        .notification{
            overflow-x: hidden;
            overflow-y: auto;
            position: fixed;
            z-index: 1050;
            -webkit-overflow-scrolling: touch;
            outline: 0;
            right: 30px;
            bottom: 20px;
            width: 250px;
        }
    </style>
</head>
<body>
    <div id="app" ng-app="myApp">

        <header class="bg-dark dk header navbar navbar-fixed-top-xs">
            <div class="navbar-header aside-md"> 
                <a href="#" class="navbar-brand" data-toggle="fullscreen">
                    <img src="/images/logo.png" class="m-r-sm">Admin
                </a>
            </div>  
            <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle">{{ Auth::user()->name }} <b class="caret"></b></a> 
                    <ul class="dropdown-menu animated fadeInRight">
                        <span class="arrow top"></span> 
                        <li><a href="#">Settings</a></li> <li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form></li>
                    </ul>
                </li>
            </ul>
        </header>
        <section class="hbox stretch"> 
            <!-- .aside --> 
            <aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav"> 
                <section class="vbox"> 
                    <header class="header bg-primary lter text-center clearfix"> 
                        <div class="btn-group"> 
                            <a href="#!addMatch">
                                <button type="button" class="btn btn-sm btn-dark btn-icon"><i class="fa fa-plus"></i></button>
                                <div class="btn-group hidden-nav-xs"> 
                                    <button type="button" class="btn btn-sm btn-primary"> Ajouter Match</button> 
                                </div>
                            </a> 
                        </div> 
                    </header> 
                    <section class="w-f scrollable"> 
                        <div class="slimScrollDiv">
                            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333" style="overflow: hidden; width: auto; height: 293px;"> 
                                <!-- nav --> 
                                <nav class="nav-primary hidden-xs"> 
                                    <ul class="nav">    
                                        <li> 
                                            <a> 
                                                <i class="fa fa-bolt icon"> <b class="bg-info"></b> </i> 
                                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> 
                                                <span>Matchs</span> 
                                            </a> 
                                            <ul class="nav lt"> 
                                                <li> 
                                                    <a href="#!addMatch"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Ajouter Match</span> 
                                                    </a> 
                                                </li> 
                                                <li > 
                                                    <a href="#!matchs"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Liste Matchs</span> 
                                                    </a> 
                                                </li>  
                                            </ul> 
                                        </li>  
                                        <li> 
                                            <a> 
                                                <i class="fa fa-bolt icon"> <b class="bg-info"></b> </i> 
                                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> 
                                                <span>Videos</span> 
                                            </a> 
                                            <ul class="nav lt"> 
                                                <li> 
                                                    <a href="#!addVideo"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Ajouter Video</span> 
                                                    </a> 
                                                </li> 
                                                <li > 
                                                    <a href="#!videos"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Liste Videos</span> 
                                                    </a> 
                                                </li>  
                                            </ul> 
                                        </li>  
                                        <li> 
                                            <a> 
                                                <i class="fa fa-bullseye icon"> <b class="bg-success"></b> </i> 
                                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> 
                                                <span>Chaînes</span> 
                                            </a> 
                                            <ul class="nav lt"> 
                                                <li> 
                                                    <a href="#!addChannel"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Ajouter Chaîne</span> 
                                                    </a> 
                                                </li> 
                                                <li > 
                                                    <a href="#!channels"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Liste Chaînes</span> 
                                                    </a> 
                                                </li>  
                                            </ul> 
                                        </li>  
                                        <li> 
                                            <a> 
                                                <i class="fa fa-trophy icon"> <b class="bg-danger"></b> </i> 
                                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> 
                                                <span>Ligues</span> 
                                            </a> 
                                            <ul class="nav lt"> 
                                                <li> 
                                                    <a href="#!addLeague"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Ajouter Ligue</span> 
                                                    </a> 
                                                </li> 
                                                <li > 
                                                    <a href="#!leagues"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Liste Ligues</span> 
                                                    </a> 
                                                </li>  
                                            </ul> 
                                        </li>  
                                        <li> 
                                            <a> 
                                                <i class="fa fa-dribbble icon"> <b class="bg-warning"></b> </i> 
                                                <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> 
                                                <span>Equipes</span> 
                                            </a> 
                                            <ul class="nav lt"> 
                                                <li> 
                                                    <a href="#!addTeam"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Ajouter Equipe</span> 
                                                    </a> 
                                                </li> 
                                                <li > 
                                                    <a href="#!teams"> 
                                                        <i class="fa fa-angle-right"></i> 
                                                        <span>Liste Equipes</span> 
                                                    </a> 
                                                </li>  
                                            </ul> 
                                        </li>  
                                    </ul> 
                                </nav> 
                                <!-- / nav --> 
                            </div>
                            <div class="slimScrollBar" style="background: rgb(51, 51, 51) none repeat scroll 0% 0%; width: 5px; position: absolute; top: 0px; opacity: 0.4; border-radius: 7px; z-index: 99; right: 0px; height: 245.283px; display: block;"></div>
                            <div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 0px;"></div>
                        </div>
                    </section>
                </section>
            </aside>   
            <section id="content">
                <section class="vbox">
                    <section class="scrollable padder">               
                        <div ng-view=""></div>
                    </section>                 
                </section>
                <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
            </section>
        </section>   
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('app/lib/angular.min.js') }}"></script>
    <script src="{{ asset('app/lib/angular-route.min.js') }}"></script>
    <script src="{{ asset('app/adminApp.js') }}"></script>
    <script src="{{ asset('app/controllers/administration/teamController.js') }}"></script>
    <script src="{{ asset('app/controllers/administration/leagueController.js') }}"></script>
    <script src="{{ asset('app/controllers/administration/channelController.js') }}"></script>
    <script src="{{ asset('app/controllers/administration/matchController.js') }}"></script>
    <script src="{{ asset('app/controllers/administration/videoController.js') }}"></script>

</body>
</html>
