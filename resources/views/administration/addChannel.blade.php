<div class="m-t-md col-sm-offset-3 col-sm-6"> 
    <form class="form-horizontal" data-validate="parsley" ng-submit="addChannel()" method="post" enctype="multipart/form-data">  
        <section class="panel panel-default"> 
            <header class="panel-heading"> <strong>Ajouter Chaîne</strong> </header> 
            <div class="panel-body"> 
                <div class="notification alert alert-<% alert.iShow %> alert-block" ng-if="alert.iShow">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <h4><i class="fa fa-bell-alt"></i>Notification!</h4> 
                    <p><% alert.msg %></p> 
                </div>
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Nom Chaîne <span class="text-danger">*</span></label> 
                    <div class="col-sm-9"> 
                        <input ng-model="channel.name" class="form-control parsley-validated" data-type="text" required placeholder="Nom Chaîne" type="text"> 
                    </div> 
                </div> 
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Logo</label> 
                    <div class="col-sm-9"> 
                    <input id="logo" type="file"> 
                    </div> 
                </div> 
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Code <span class="text-danger">*</span></label> 
                    <div class="col-sm-9"> 
                        <textarea ng-model="channel.code" class="form-control parsley-validated" rows="6" data-minwords="6" required placeholder="Code"></textarea>                    
                    </div> 
                </div> 
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Description <span class="text-danger">*</span></label> 
                    <div class="col-sm-9"> 
                        <textarea ng-model="channel.description" class="form-control parsley-validated" rows="4" data-minwords="6" required placeholder="Description"></textarea>                    
                    </div> 
                </div> 
            </div>
            <footer class="panel-footer text-right bg-light lter"> 
                <button type="submit" class="btn btn-success btn-s-xs">Ajouter</button> 
            </footer>
        </section>
    </form>
</div>