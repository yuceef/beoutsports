<div class="m-t-md col-sm-offset-3 col-sm-6"> 
    <form class="form-horizontal" data-validate="parsley" ng-submit="addMatch()" method="post" enctype="multipart/form-data">  
        <section class="panel panel-default"> 
            <header class="panel-heading"> <strong>Ajouter Match</strong> </header> 
            <div class="panel-body"> 
                <div class="notification alert alert-<% alert.iShow %> alert-block" ng-if="alert.iShow">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <h4><i class="fa fa-bell-alt"></i>Notification!</h4> 
                    <p><% alert.msg %></p> 
                </div>
                <% match %>
                <div class="form-group"> 
                  	<label class="col-sm-3 control-label">Equipe 1<span class="text-danger">*</span></label> 
                  	<div class="col-sm-9 row"> 
						<div class="row col-sm-9">
                            <div class="col-sm-4"> 
                            <input class="form-control" type="text" ng-model="teamSe1" >
                            </div> 
                            <div class="col-sm-8"> 
                                <select class="form-control" ng-model="match.home_team" required>
                                    <option ng-repeat="team in teams | filter:teamSe1:false" value="<%team.id%>"><%team.name%></option>
                                </select>
                    	    </div>
                    	</div>
						<div class="col-sm-3"> 
							<input ng-model="match.home_goal" class="form-control parsley-validated" data-type="number"  type="number"> 
						</div> 
                    </div> 
                </div> 
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                  	<label class="col-sm-3 control-label">Equipe 2 <span class="text-danger">*</span></label> 
                      <div class="col-sm-9 row"> 
						<div class="row col-sm-9">
                            <div class="col-sm-4"> 
                            <input class="form-control" type="text" ng-model="teamSe2" >
                            </div> 
                            <div class="col-sm-8"> 
                                <select class="form-control" ng-model="match.away_team" required>
                                    <option ng-repeat="team in teams | filter:teamSe2:false" value="<%team.id%>"><%team.name%></option>
                                </select>
                    	    </div>
                    	</div>
						<div class="col-sm-3"> 
							<input ng-model="match.away_goal" class="form-control parsley-validated" data-type="number" type="number"> 
						</div> 
                    </div> 
                </div> 
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Ligue <span class="text-danger">*</span></label> 
                    <div class="row col-sm-9">
                        <div class="col-sm-4"> 
                            <input class="form-control" type="text" ng-model="leagueSe" >
                        </div> 
                        <div class="col-sm-8"> 
                            <select class="form-control" ng-model="match.league_id" required>
                                <option ng-repeat="league in leagues | filter:leagueSe:false" value="<%league.id%>"><%league.name%></option>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Journée</label> 
                    <div class="col-sm-9">
                        <input class="form-control" type="text" ng-model="match.day">
                    </div>
                </div>  
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Date <span class="text-danger">*</span></label> 
                    <div class="row col-sm-9">
                        <div class="col-sm-6"> 
                            <input class="form-control" type="date" ng-model="dateM"  required>
                        </div> 
                        <div class="col-sm-4"> 
                            <input class="form-control" type="time" ng-model="timeM" required>
                        </div> 
                        
                    </div>
                </div>                
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Channels</label> 
                    <div class="col-sm-9">
                        <select multiple="" class="form-control" ng-model="match.channels"> 
                            <option ng-repeat="channel in channels" value="<%channel.id%>"><%channel.name%></option>
                        </select>
                    </div>
                </div>                
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Video</label> 
                    <div class="col-sm-9">
                        <input class="form-control" type="text" ng-model="match.video">
                    </div>
                </div>                
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Stadium</label> 
                    <div class="col-sm-9">
                        <input class="form-control" type="text" ng-model="match.stadium">
                    </div>
                </div>                
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Extra</label> 
                    <div class="col-sm-9">
                        <input name="Extra" ng-model="match.isExtra" type="radio" value="0"> OFF
                        <input name="Extra" ng-model="match.isExtra" type="radio" value="1"> ON
                    </div>
                </div>                
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Penalties</label> 
                    <div class="col-sm-9">
                        <input name="Penalty" ng-model="match.isPenalty" type="radio" value="0"> OFF
                        <input name="Penalty" ng-model="match.isPenalty" type="radio" value="1"> ON
                    </div>
                </div>                
                <div class="line line-dashed line-lg pull-in"></div> 
                <div class="form-group"> 
                    <label class="col-sm-3 control-label">Description <span class="text-danger">*</span></label> 
                    <div class="col-sm-9"> 
                        <textarea ng-model="match.description" class="form-control parsley-validated" rows="6" data-minwords="6" required placeholder="Description"></textarea>                    
                    </div> 
                </div> 
            </div>
            <footer class="panel-footer text-right bg-light lter"> 
                <button type="submit" class="btn btn-success btn-s-xs">Ajouter</button> 
            </footer>
        </section>
    </form>
</div>