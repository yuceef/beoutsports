<div class="m-t-md col-sm-offset-2 col-sm-8 ng-scope">
    <section class="panel panel-default"> 
        <header class="panel-heading"> Liste Ligues </header> 
        <div class="table-responsive"> 
            <table class="table table-striped datagrid m-b-sm"> 
                <thead> 
                    <tr> 
                        <th colspan="5"> 
                            <div class="row"> 
                                <div class="col-sm-8 m-t-xs m-b-xs"></div>
                                <div class="col-sm-4 m-t-xs m-b-xs"> 
                                    <div class="input-group search datagrid-search"> 
                                        <input class="input-sm form-control" ng-model="searchLeague" placeholder="Search" type="text">
                                        <div class="input-group-btn"> 
                                            <button class="btn btn-default btn-sm">
                                                <i class="fa fa-search"></i>
                                            </button> 
                                        </div> 
                                    </div> 
                                </div> 
                            </div> 
                        </th> 
                    </tr> 
                    <tr>
                        <th class="sortable" scope="col">Logo</th>
                        <th class="sortable" scope="col">Nom</th>
                        <th class="sortable" scope="col">Saison</th>
                        <th class="sortable" scope="col">Action</th>
                    </tr>
                </thead> 
                <tbody>
                    <tr ng-repeat="league in leagues | filter:searchLeague:false ">
                        <td><img src="<% league.logo %>" ng-alt="<% league.name %>" height="35px"></td>
                        <td class="font-weight-bold"><h5><% league.name %></h5></td>
                        <td class="font-weight-bold"><h5><% league.saison %></h5></td>
                        <td>
                            <button class="btn btn-success" ng-click="upShowLeague(league.id)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-danger" ng-click="deleteLeague(league.id)" ><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table> 
        </div>
    </section>
</div>
<div class="notification alert alert-<% alert.iShow %> alert-block" ng-if="alert.iShow">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <h4><i class="fa fa-bell-alt"></i>Notification!</h4> 
    <p><% alert.msg %></p> 
</div>
<div class="modal in"style="display: block;" ng-if="isModel">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" ng-click="closeModel()">×</button> 
                <h4 class="modal-title">Modifier Equipe</h4> 
            </div> 
            <div class="modal-body">
                <div class="form-horizontal"> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Nom Ligue <span class="text-danger">*</span></label> 
                        <div class="col-sm-9"> 
                            <input ng-model="league.name" class="form-control parsley-validated" data-type="text" required placeholder="Nom Ligue" type="text"> 
                        </div> 
                    </div> 
                    <div class="line line-dashed line-lg pull-in"></div> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Saison</label> 
                        <div class="col-sm-9"> 
                            <input ng-model="league.saison" class="form-control parsley-validated" data-type="text" required placeholder="Saison" type="text"> 
                        </div> 
                    </div> 
                    <div class="line line-dashed line-lg pull-in"></div> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Logo</label> 
                        <div class="col-sm-9"> 
                        <input id="logo" type="file"> 
                        </div> 
                    </div> 
                    <div class="line line-dashed line-lg pull-in"></div> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Description <span class="text-danger">*</span></label> 
                        <div class="col-sm-9"> 
                            <textarea ng-model="league.description" class="form-control parsley-validated" rows="6" data-minwords="6" required placeholder="Description"></textarea>                    
                        </div> 
                    </div> 
                </div>
            </div>
            <div class="modal-footer"> 
                <a class="btn btn-default" ng-click="closeModel()">Close</a> 
                <a class="btn btn-primary" ng-click="upLeague(league.id)">Save</a> 
            </div> 
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal-backdrop  in" ng-if="isModel"></div>
