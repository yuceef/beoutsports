<style>
.ino{
    background: #ffbc1a !important
}
tr.ino:nth-child(2n+1) > th {
    background-color: #ffbc1a !important;
}
 tr.ino:nth-child(2n+1) > td, tr.ino:nth-child(2n+1) > th {
    background-color: #ffbc1a !important;
}
</style>
<div class="m-t-md col-sm-offset-2 col-sm-8 ng-scope">
    <section class="panel panel-default"> 
        <header class="panel-heading"> Liste Matchs </header> 
        <div class="table-responsive"> 
            <table class="table table-striped m-b-sm"> 
                <thead> 
                    <tr> 
                        <th colspan="4"> 
                            <div class="row"> 
                                <div class="col-sm-3 m-t-xs m-b-xs">
                                    <input type="date" class="input-sm form-control" ng-model="dateSer" ng-change="getMatchs(dateSer)">
                                </div>
                                <div class="col-sm-4 m-t-xs m-b-xs">
                                </div>
                                <div class="col-sm-4 m-t-xs m-b-xs"> 
                                    <div class="input-group search datagrid-search"> 
                                        <input class="input-sm form-control" ng-model="searchMatch" placeholder="Search" type="text">
                                        <div class="input-group-btn"> 
                                            <button class="btn btn-default btn-sm">
                                                <i class="fa fa-search"></i>
                                            </button> 
                                        </div> 
                                    </div> 
                                </div> 
                            </div> 
                        </th> 
                    </tr> 
                    <tr>
                        <th class="sortable" scope="col">Ligue</th>
                        <th class="sortable" scope="col">Match</th>
                        <th class="sortable" scope="col">Date</th>
                        <th class="sortable" scope="col">Action</th>
                    </tr>
                </thead> 
                <tbody>
                    <tr ng-repeat="match in matchs | filter:searchMatch:false "  ng-class="{ino: isNovideo(match)}">
                        <td><img src="<% getLeague(match.league_id) %>"  height="35px"></td>
                        <td>
                            <img src="<% getTeam(match.home_team) %>"  height="35px"> <span class="font-bold">VS</span> <img src="<% getTeam(match.away_team) %>"  height="35px">
                        </td>
                        <td class="font-weight-bold"><h5><% match.date %></h5></td>
                        <td>
                            <button class="btn btn-success" ng-click="upShowMatch(match.id)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-danger" ng-click="deleteMatch(match.id)" ><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table> 
        </div>
    </section>
</div>
<div class="notification alert alert-<% alert.iShow %> alert-block" ng-if="alert.iShow">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <h4><i class="fa fa-bell-alt"></i>Notification!</h4> 
    <p><% alert.msg %></p> 
</div>
<div class="modal in"style="display: block;" ng-if="isModel">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" ng-click="closeModel()">×</button> 
                <h4 class="modal-title">Modifier Match</h4> 
            </div> 
            <div class="modal-body">
                <div class="form-horizontal"> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Resultat<span class="text-danger">*</span></label> 
                        <div class="col-sm-9 row"> 
                            <div class="col-sm-6"> 
                                <input ng-model="match.home_goal" class="form-control parsley-validated" data-type="number"  type="number"> 
                            </div> 
                            <div class="col-sm-6"> 
                                <input ng-model="match.away_goal" class="form-control parsley-validated" data-type="number"  type="number"> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="line line-dashed line-lg pull-in"></div> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Date <span class="text-danger">*</span></label> 
                        <div class="row col-sm-9">
                            <div class="col-sm-6"> 
                                <input class="form-control" type="date" ng-model="match.dateM"  >
                            </div> 
                            <div class="col-sm-4"> 
                                <input class="form-control" type="time" ng-model="match.timeM" >
                            </div>     
                        </div>
                    </div>                
                    <div class="line line-dashed line-lg pull-in"></div> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Channels</label> 
                        <div class="col-sm-9">
                            <select multiple="" class="form-control" ng-model="match.channels"> 
                                <option ng-repeat="channel in channels" value="<%channel.id%>"><%channel.name%></option>
                            </select>
                        </div>
                    </div>                
                    <div class="line line-dashed line-lg pull-in"></div> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Video</label> 
                        <div class="col-sm-9">
                            <input class="form-control" type="text" ng-model="match.video">
                        </div>
                    </div>                             
                    <div class="line line-dashed line-lg pull-in"></div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Extra</label> 
                        <div class="row col-sm-9">
                            <div class="col-sm-6"> 
                                Extra
                                <input name="Extra" ng-model="match.isExtra" type="radio" value="0"> OFF
                                <input name="Extra" ng-model="match.isExtra" type="radio" value="1"> ON
                            </div> 
                            <div class="col-sm-6"> 
                                Penalties
                                <input name="Penalty" ng-model="match.isPenalty" type="radio" value="0"> OFF
                                <input name="Penalty" ng-model="match.isPenalty" type="radio" value="1"> ON
                             </div>     
                        </div> 
                    </div>  
                    <div class="line line-dashed line-lg pull-in"></div> 
                    <div class="form-group"> 
                        <label class="col-sm-3 control-label">Description <span class="text-danger">*</span></label> 
                        <div class="col-sm-9"> 
                            <textarea ng-model="match.description" class="form-control parsley-validated" rows="4" data-minwords="4" required placeholder="Description"></textarea>                    
                        </div> 
                    </div> 
                </div>
            </div>
            <div class="modal-footer"> 
                <a class="btn btn-default" ng-click="closeModel()">Close</a> 
                <a class="btn btn-primary" ng-click="upMatch(match.id)">Save</a> 
            </div> 
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal-backdrop  in" ng-if="isModel"></div>
