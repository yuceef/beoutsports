<div class="container">
    <img src="/images/banner.jpg" height="300" width="100%" alt="">
</div>
<div class="container bg-white p-4">
    <h4 ><span class="calendar-heading">مباريات  <% titre %> :</span></h4>
    <div class="calendar-filter">
        <ul class="calendar-buttons col-md-12 row">
            <li class="col-md-2-3 pb-2">
                <select class="col-md-12 form-control" ng-model="selectLeague">
                    <option value="">كل الدوريات</option>
                    <option ng-repeat="league in leagues" value="<%league.name%>"><%league.name%></option>
                </select>
            </li>
            <li class="calendar-live col-md-2 pb-2">
                <a href="#!calendar/now" class="form-control" title="جارية حاليا" role="button">جارية حاليا</a>
            </li>
            <li class="col-md-4 pb-2">
            </li>
            <li class="col-md-3-4 pb-2">
                <input class="form-control" type="date" ng-model="dateSearch" value="21_05_2018" />
            </li>
            <li class="col-md-1 pb-2">
                <button class="btn btn-default" ng-click="direction(dateSearch,true)">اذهب</button>
            </li>
        </ul>
    </div>
    <table class="table table-hover table-responsive-md border rounded">
        <tbody>
            <tr ng-repeat="match in matchs | filter:selectLeague:false" class='clickable-row' ng-click="direction('matchs/'+match.id+'/'+match.home.name+'vs'+match.away.name)">
                <td ng-if="isLive(match.date) != 'live'"><time class="calendar-league-time timezone time"><%timeMatch(match.date)%> GMT</time></td>
                <td  ng-if="isLive(match.date) == 'live'">
                    <div style="flex: 0 0 55px;">
                        <div class="livescore-timer" data-percent="87" data-bar-color="#C60C30" style="color:#C60C30">
                            <span class="time_live_<%match.id%>_xx">'<%timeOfMatch(match.date)%></span>
                        </div>
                    </div>
                </td>
                <td><img src="<%match.league.logo%>" alt="<%match.league.name%>"></td>
                <td><%match.home.name%></td>
                <td><img src="<%match.home.logo%>" alt="<%match.home.name%>"></td>
                <td ng-if="match.home_goal!=null && match.away_goal!=null">
                    <button class="btn btn-sm"  ng-class="{'btn-success': match.home_goal>match.away_goal,'btn-danger': match.home_goal<match.away_goal,'btn-dark': match.home_goal==match.away_goal}"><%match.home_goal%></button>
                    <button class="btn btn-sm"  ng-class="{'btn-success': match.home_goal<match.away_goal,'btn-danger': match.home_goal>match.away_goal,'btn-dark': match.home_goal==match.away_goal}"><%match.away_goal%></button>
                </td>
                <td ng-if="match.home_goal==null || match.away_goal==null">
                    <button class="btn btn-default btn-sm">-</button>
                    <button class="btn btn-default btn-sm">-</button>
                </td>
                <td><img src="<%match.away.logo%>" alt="<%match.away.name%>"></td>
                <td><%match.away.name%></td>
                <td><img src="/images/icons/stadium.svg" alt=""><small> <%match.stadium%></small></td>
                <td>
                    <a ng-if="isLive(match.date)=='apres'" title="شاهد ملخص <%match.home.name%> vs <%match.away.name%>" class="calendar-league-watch" href="#!matchs/<%match.id%>/<%match.home.name%>vs<%match.away.name%>">
                    <img src="/images/icons/play.png" alt=""><small>ملخص المباراة</small>
                    </a>                                
                    <a ng-if="isLive(match.date)!='apres'" title="شاهد <%match.home.name%> vs <%match.away.name%>" class="calendar-league-watch" href="#!matchs/<%match.id%>/<%match.home.name%>vs<%match.away.name%>">
                    <img src="/images/icons/play.png" alt=""><small>شاهد المباراة </small>
                    </a>                                
                </td>
                <td>
                <iframe src="https://www.facebook.com/plugins/share_button.php?href={{ url('/') }}&layout=button_count&size=small&mobile_iframe=true&width=85&height=20&appId" width="85" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>                               
                </td>
            </tr>
        </tbody>
    </table>
</div>
