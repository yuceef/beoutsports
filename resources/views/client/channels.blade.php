<div class="container bg-white p-4 row">
<h4 ><span class="calendar-heading">القنوات :</span></h4>
    <div class="col-sm-2" ng-repeat="channel in channels">
        <a href="/channels/<%channel.ref%>.html">
            <img src="<%channel.logo%>" alt="<%channel.name%>" title="<%channel.name%>">
        </a>
    </div>
</div>