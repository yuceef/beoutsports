<div class="container bg-white ">
    <div class="row match-details">
        <div class="col-sm-3">
            <div class="match-details-rightside">
                <div class="match-details-fanion">
                    <img src="<%match.home.logo%>" rel="<%match.home.name%>">
                </div>
                <h3 class="match-details-teamname"><%match.home.name%></h3>
            </div>        
        </div>
        <div dir="ltr" class="col-sm-6">
            <div class="match-details-middle">
                <h2  class="match-details-middle__round offset-3 col-6">
                    <span class="timezone time" data-format="dddd D MMMM YYYY على الساعة HH:mm " data-value="<%match.date%>"><%match.date%> GMT</span>    
                </h2>
                <div dir="rtl" class="match-details-score">   
                    <div>
                        <span class="match-details-scoreplaceholder"><%match.home_goal%></span>
                        <span class="match-details-scoreseparator">:</span>
                        <span class="match-details-scoreplaceholder"><%match.away_goal%></span>
                    </div>
                    <span class="match-details-matchstatus">
                        <img src="/images/icons/chrono.svg"> '<%getTimeLive(match.date)%>  
                    </span>
                </div>       
           </div>       
        </div>
        <div class="col-sm-3">
            <div class="match-details-rightside">
                <div class="match-details-fanion">
                    ><img src="<%match.away.logo%>" rel="<%match.away.name%>">
                </div>
                <h3 class="match-details-teamname"><%match.away.name%></h3>
            </div>    
        </div>
        <div class="match-details__timeline-wrapper" ng-if="isLive(match.date)">
            <div class="match-details__timeline-progress" style="width:calc( <%getTimeLive(match.date)%>% + 55px );">
                <span class="match-details__timeline-progress-minute">'<%getTimeLive(match.date)%> 
                <span class="sonar-wave"></span>
                </span>
            </div>
            <div class="match-details__timeline-bar">  
                <span class="match-details__timeline-large-pill" style="left:-35px;">0'</span>
                <div style="width:430px;height:5px;"></div> 
                <span class="match-details__timeline-large-pill" style="right:-35px;">90'</span>   
            </div>
        </div>
    </div>

    <div class="bg-white justify-content-center text-center" dir="ltr" ng-if="getTimeLive(match.date) == 'انتهت'">
        <h1 dir="rtl" >ملخص مباراة <%match.home.name%> vs <%match.away.name%></h1>
        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FBeOutSportsPage%2F&width=450&layout=standard&action=like&size=small&show_faces=true&share=true&height=80&appId" width="450" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
        <div class="justify-content-center pb-4">
            <div ng-bind-html="videoCode | renderHTMLCorrectly"></div>
        </div>
    </div>
    <div class="bg-white justify-content-center text-center" dir="ltr" ng-if="getTimeLive(match.date) != 'انتهت'">
        <h3 dir="rtl" >مشاهدة مباراة <%match.home.name%> و <%match.away.name%> بث مباشر  <%match.date%> GMT <%match.league.name%> </h3>
        <nav class="pagination p-4 justify-content-center text-center" >
            <ul class="pagination">
                <li class="page-item" ng-repeat="channel in match.infoChannel"  ng-class="{'active':myRef == channel.ref}" >
                    <a class="page-link" ng-click="changeCode(channel.ref)"><img src="<%channel.logo%>" height="35" alt=""> <%channel.name%></a>
                </li>
            </ul>
        </nav>
        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FBeOutSportsPage%2F&width=450&layout=standard&action=like&size=small&show_faces=true&share=true&height=80&appId" width="450" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
        <div class="justify-content-center pb-4">
            <div ng-bind-html="liveCode | renderHTMLCorrectly"></div>
        </div>
    </div>
    <p class="justify-content-center text-center"><%match.description%></p>
    <%match.vue%> زيارة
</div>
