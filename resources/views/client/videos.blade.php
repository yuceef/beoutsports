<div class="container text-center bg-white p-3">
    <ul class="videos-page__categories-entries" >
        <li class="videos-page__entry-element" ng-repeat="video in videos">
        <div class="article-header">
            <div class="article-image">
                <a href="#!video/<%video.id%>/<%video.title%>" title="<% video.title %>">
                    <img src="//images.elbotola.com/svg/play-button.svg" class="video-img__play-btn-medium">
                    <img src="https://i.ytimg.com/vi/<% getCode(video.url) %>/maxresdefault.jpg" class="cover" alt="">
                </a>
            </div>
        </div>
        <div class="article-content">
            <div class="article-main">
                <h5 class="article-heading">
                    <a href="#!video/<%video.id%>/<%video.title%>" title="<% video.title %>"><% video.title %></a>
                </h5>
            </div>
        </div>
        <a class="article-anchor" href="#!video/<%video.id%>/<%video.title%>" title="<% video.title %>"></a>
        </li>
    <div class="clearfix"></div>
    </ul>
    <button ng-click="getVideos(pg)" class="btn btn-primary">المزيد من الفيديوات</button>
</div>