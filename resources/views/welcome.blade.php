<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="rtl" ng-app="myApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="بي اوت سبورس, BeOutSports, موقع بي اوت سبورس,كورة اون لاين, كورة اونلاين, Kora Online, يلا شوت, يالا شوت, بث مباشر, مباريات اليوم, نقل مباشر, نقل مباراة, مقابلة, لعبة, جدول مباريات اليوم, مشاهدة مباراة, كورة لايف, يلا شوت ستورز, Yalla Shoot, Kora Live, Kooora, koora, نتيجة مباراة, نقل مباشر, كول كورة, كوول كورة, Kool Cora, www yalla-shoot com, يلاكورة, يلا كورة, بين سبورت, بيان سبورت, بينسبورت, القنوات الناقلة, موعد مباراة, ماتش لايف, Match Live,live,match,bein,bein sports,score,football,tennis,liga,euro,kooora,eurosport,goal,but,messi,ronaldo,barcelona,real madrid,en direct,sport,live match,online match,online game,نقل مباشر,مباراة,مباريات,الشوالي,بين سبورت,الشرينغ,أونلاين,فرق,منتخبات,كووورة,أهداف , russia 2018, world cup 2018" name="keywords">
        <meta content="موقع BeOutSports لمشاهدة مباريات كاس العالم 2018 فى روسيا بث مباشر باعلي جودة وبدون تقطيع علي بي ان سبورت بث مباشر بيان سبورت ماكس مباشرة موقع يلا شوت كورة اون لاين كووورة لايف مباريات اليوم نقل مباشر مباراة مصر مباراة السعودية اليوم مباراة المغرب اليوم" name="description">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="google-site-verification" content="M17aUDqTxvBbmhUxj_Ay0cSsOwiWxoSi0HtVzJBrke4" />
        
        <title>{{ config('app.name', 'Laravel') }} - <%titre%></title>

        <!-- Styles -->
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body id="app" class="scrolled">
        <nav class="navbar navbar-expand-md navbar-dark navBg navbar-laravel">
            <div class="container">
                <a class="navbar-brand ml-auto mr-0" href="{{ url('/') }}">
                    <img src="/images/logo.png" height="40px" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" >
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto ml-auto">
                        <li>
                            <form class="navbar-search">
                                <span class="twitter-typeahead" style="position: relative; display: inline-block; direction: rtl;">
                                    <input type="search" placeholder="بحث..." class="form-control" autofocus>
                                </span>
                                <button type="submit"></button>
                            </form>                        
                        </li>
                    </ul>
                    <ul class="navbar-nav navbar-language mr-0 ml-0">
                    @foreach($leagues as $league)
                        <li><a href="#!leagues/{{$league->id}}/{{str_replace(" ","_",$league->name)}}" title="{{$league->name}}"><img src="{{$league->logo}}" class="text-center"></a></li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </nav>
        <div id="js-top-bar">
            <nav class="navbar navbar-expand-md navigation">
                <div class="container">
                    <ul class="navigation-calendar ml-auto">
                        <li><a href="#!calendar/yesterday">أمس</a></li>
                        <li>
                            <a class="navbar-live el-pointer " href="#!calendar/now">live<span>{{$liveNb}}</span></a>
                        </li>
                        <li><a href="#!calendar/today">اليوم</a></li>
                        <li><a href="#!calendar/tomorrow">الغد</a></li>
                    </ul>
                    <div class="navbar-collapse collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto navigation-list">
                            <li><a href="{{ url('/') }}">الرئيسية</a></li>
                            <li><a href="#!channels">القنوات</a></li>
                            <li><a href="#!calendar/resume">ملخص المباريات</a></li>
                            <li><a href="#!videos">فيديوهات</a></li>
                            <li style="background-color:#f60;"><a style="color: #000" href="#!App">تحميل تطبيق beOut</a></li>
                            <li><a href="#!contact">اتصل بنا</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div ng-view=""></div>
            <div class="container">
                <h5 class="text-center navBg text-white p-2">BeOutSports © 2018 | جميع الحقوق محفوظة</h5>
                <!-- Histats.com  START (html only)-->
                <a href="/" alt="page hit counter" target="_blank" >
                <embed src="http://s10.histats.com/1044.swf"  flashvars="jver=1&acsid=4077518&domi=4"  quality="high"  width="200" height="30" name="1044.swf"  align="middle" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" /></a>
                <img  src="//sstatic1.histats.com/0.gif?4077518&101" alt="free website hit counter" border="0">
                <!-- Histats.com  END  -->
            </div>
        </div> 
              
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('app/lib/angular.min.js') }}"></script>
    <script src="{{ asset('app/lib/angular-route.min.js') }}"></script>
    <script src="{{ asset('app/app.js') }}"></script>
    <script src="{{ asset('app/controllers/client/calendarController.js') }}"></script>
    <script src="{{ asset('app/controllers/client/matchController.js') }}"></script>
    <script src="{{ asset('app/controllers/client/channelController.js') }}"></script>
    <script src="{{ asset('app/controllers/client/videosController.js') }}"></script>
    <script src="{{ asset('app/controllers/client/videoController.js') }}"></script>
    <script>
        $( document ).ready(function() {
            setInterval(function time(){
            var d = new Date();
            var hours = 24 - d.getHours();
            var min = 60 - d.getMinutes();
            if((min + '').length == 1){
                min = '0' + min;
            }
            var sec = 60 - d.getSeconds();
            if((sec + '').length == 1){
                    sec = '0' + sec;
            }
            jQuery('#timer').html(hours+":"+min+":"+sec);
            }, 1000); 
        });
    </script>
</body>
</html>
