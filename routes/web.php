<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Back end
Route::resource('/apiWeb/teams', 'TeamController');
Route::resource('/apiWeb/leagues', 'LeagueController');
Route::resource('/apiWeb/channels', 'ChannelController');
Route::resource('/apiWeb/matchs', 'MatchController');
Route::resource('/apiWeb/videos', 'VideoController');
Route::get('/apiWeb/matchs/{by}/{val}', 'MatchController@search');
Route::get('/apiWeb/videos/page/{val}', 'VideoController@pagination');

// Template
Route::get('/template/{type}/{view}',function($type,$view){
    return view($type."/".$view);
});

// Front end
Route::get('/adminstration', 'HomeController@adminstration');
Route::get('/adminstration/{var}', 'HomeController@adminstration')->where('var','[^*]*');
Route::get('/{var}', 'HomeController@index')->where('var','[^*]*');