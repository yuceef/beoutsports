<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('home_team');
            $table->integer('away_team');
            $table->integer('home_goal')->nullable();
            $table->integer('away_goal')->nullable();
            $table->integer('league_id');
            $table->integer('day')->nullable();
            $table->string('stadium')->nullable();
            $table->dateTime('date');
            $table->string('channels')->nullable();            
            $table->string('video')->nullable();  
            $table->boolean('isExtra')->default(false);          
            $table->boolean('isPenalty')->default(false);          
            $table->text('description')->nullable();	
            $table->integer('vue')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
