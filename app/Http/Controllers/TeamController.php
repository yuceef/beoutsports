<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class TeamController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::orderBy('name', 'asc')->get();
        return $this->sendResponse($teams->toArray(), 'Teams retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        $input = $request->all();
        $filename = $_FILES['logo']['name'];
        $validator = Validator::make($input, [
            'name' => 'required|string|unique:teams|max:120',
            'short_name' => 'required|string|max:20',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required|string|min:5'
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        $path = $request->file('logo')->storeAs(
            'public/teams', $request->short_name."_".date("d_m_Y_H_s").".".pathinfo($filename, PATHINFO_EXTENSION)
        );
        $team = new Team;
        $team->name = $input['name'];
        $team->short_name = $input['short_name'];
        $team->nationality = $input['nationality'];
        $team->logo = str_replace("public","/storage",$path);
        $team->description = $input['description'];
        $team->save();
        return $this->sendResponse($team, 'L\'équipe '.$team->name.'a été bien ajouter.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = Team::find($id);
        if (is_null($team))
            return $this->sendError('Team not found.');
        return $this->sendResponse($team->toArray(), 'L\'équipe '.$team->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::find($id);
        if (is_null($team))
            return $this->sendError('Team not found.');
        $input = $request->all();
        if(isset($_FILES['logo']))$filename = $_FILES['logo']['name'];
        $validator = Validator::make($input, [
            'name' => 'required|string|max:120|unique:teams,id,'.$id,
            'short_name' => 'required|string|max:20',
            'description' => 'required|string|min:5'
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        if(isset($_FILES['logo'])){
            $path = $request->file('logo')->storeAs(
                'public/teams', $request->short_name."_".date("d_m_Y_H_s").".".pathinfo($filename, PATHINFO_EXTENSION)
            );
            $team->logo = str_replace("public","/storage",$path);
        }
        $team->name = $input['name'];
        $team->short_name = $input['short_name'];
        $team->nationality = $input['nationality'];
        $team->description = $input['description'];
        $team->save();
        return $this->sendResponse($team, 'L\'équipe '.$team->name.'a été bien modifier.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::find($id);
        if (is_null($team))
            return $this->sendError('Team not found.');
        $objet = $team->name;
        Team::destroy($id);
        return $this->sendResponse($objet, 'L\'équipe '.$objet.' a été supprimer.');
    }
}
