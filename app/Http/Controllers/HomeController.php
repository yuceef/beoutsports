<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\League;
use App\Match;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('adminstration');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leagues = $this->isThetop();
        $val2 = gmdate("Y-m-d H:i:s");
        $val1 = date("Y-m-d H:i:s",strtotime($val2 . ' - 115 minutes'));
        $liveNb = Match::where("date",">",$val1)->where("date","<",$val2)->count();
        return view('welcome')->with('leagues', $leagues)
                              ->with('liveNb', $liveNb);
    }
    public function adminstration()
    {
        return view('admin');
    }
        /**
     * The Top Leagues
     * 
     * @return List of leagues 
     */
    private function isThetop()
    {
       // $league = League::where("isTop",1)->limit(6)->get();
        $league = League::All();
        return $league;
    }    
}
