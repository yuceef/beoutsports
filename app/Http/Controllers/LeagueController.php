<?php

namespace App\Http\Controllers;

use App\League;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class LeagueController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leagues = League::orderBy('id', 'desc')->get();
        return $this->sendResponse($leagues->toArray(), 'Leagues retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        $input = $request->all();
        $filename = $_FILES['logo']['name'];
        $validator = Validator::make($input, [
            'name' => 'required|string|unique:leagues|max:120',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required|string|min:5'
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        $path = $request->file('logo')->storeAs(
            'public/leagues', $filename
        );
        $league = new League;
        $league->name = $input['name'];
        $league->saison = $input['saison'];
        $league->sport_id = 0;
        $league->logo = str_replace("public","/storage",$path);
        $league->description = $input['description'];
        $league->save();
        return $this->sendResponse($league, 'Ligue '.$league->name.'a été bien ajouter .');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $league = League::find($id);
        if (is_null($league))
            return $this->sendError('League not found.');
        return $this->sendResponse($league->toArray(), 'Ligue '.$league->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function edit(League $league)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $league = League::find($id);
        if (is_null($league))
            return $this->sendError('League not found.');
        $input = $request->all();
        if(isset($_FILES['logo']))$filename = $_FILES['logo']['name'];
        $validator = Validator::make($input, [
            'name' => 'required|string|max:120|unique:leagues,id,'.$id,
            'description' => 'required|string|min:5'
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        if(isset($_FILES['logo'])){
            $path = $request->file('logo')->storeAs(
                'public/leagues', $request->name.".".pathinfo($filename, PATHINFO_EXTENSION)
            );
            $league->logo = str_replace("public","/storage",$path);
        }
        $league->name = $input['name'];
        $league->saison = $input['saison'];
        $league->sport_id = 0;
        $league->description = $input['description'];
        $league->save();
        return $this->sendResponse($league, 'Ligue '.$league->name.'a été bien modifier .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\League  $league
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $league = League::find($id);
        if (is_null($league))
            return $this->sendError('League not found.');
        $objet = $league->name;
        League::destroy($id);
        return $this->sendResponse($objet, 'Ligue '.$objet.' a été supprimer.');
    }


}
