<?php

namespace App\Http\Controllers;

use App\Match;
use App\League;
use App\Team;
use App\Channel;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class MatchController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matchs = Match::orderBy('id', 'desc')->get();
        return $this->sendResponse($matchs->toArray(), 'Matchs retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        $input = $request->all();
        $validator = Validator::make($input, [
            'home_team' => 'required',
            'away_team' => 'required',
            'league_id' => 'required',
            'date' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        
        
        $match = new Match;
        $match->home_team = $input['home_team'];
        $match->away_team = $input['away_team'];
        $match->description = $input['description'];
        if(isset($input['home_goal']))$match->home_goal = $input['home_goal'];
        if(isset($input['away_goal']))$match->away_goal = $input['away_goal'];
        $match->league_id = $input['league_id'];
        $match->date = date($input['date']);
        $match->channels = implode(",",$input['channels']);
        if(isset($input['video']))$match->video = $input['video'] ;
        if(isset($input['stadium']))$match->stadium = $input['stadium'] ;
        if(isset($input['isExtra']))$match->isExtra = $input['isExtra'] ;
        if(isset($input['isPenalty']))$match->isPenalty = $input['isPenalty'] ;
        if(isset($input['day']))$match->day = $input['day'] ;
        $match->save();
        return $this->sendResponse($match, 'Match '.$match->name.'a été bien ajouter .');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $match = Match::find($id);
        $match->vue += 1;
        $match->save();
        if (is_null($match))
            return $this->sendError('Match not found.');
        $match->league = League::find($match->league_id);
        $match->home = Team::find($match->home_team);
        $match->away = Team::find($match->away_team);
        $infoChannel = array();
        foreach (explode(",",$match->channels) as $channel) {
            $infoChannel[] = Channel::find($channel);
        }
        $match->infoChannel=$infoChannel;
        return $this->sendResponse($match->toArray(), 'Match '.$match->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function edit(Match $match)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $match = Match::find($id);
        if (is_null($match))
            return $this->sendError('Match not found.');
        
        $input = $request->all();
        $validator = Validator::make($input, [
            'home_team' => 'required',
            'away_team' => 'required',
            'league_id' => 'required',
            'description' => 'required',
            'date' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        $match->home_team = $input['home_team'];
        $match->away_team = $input['away_team'];
        if(isset($input['home_goal']))$match->home_goal = $input['home_goal'];
        if(isset($input['away_goal']))$match->away_goal = $input['away_goal'];
        $match->league_id = $input['league_id'];
        $match->description = $input['description'];
        $match->date = date($input['date']);
        $match->channels = implode(",",$input['channels']);
        if(isset($input['video']))$match->video = $input['video'] ;
        if(isset($input['stadium']))$match->stadium = $input['stadium'] ;
        if(isset($input['isExtra']))$match->isExtra = $input['isExtra'] ;
        if(isset($input['isPenalty']))$match->isPenalty = $input['isPenalty'] ;
        if(isset($input['day']))$match->day = $input['day'] ;
        $match->save();
        return $this->sendResponse($match, 'Match '.$match->name.'a été bien modifier .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Match  $match
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $match = Match::find($id);
        if (is_null($match))
            return $this->sendError('Match not found.');
        $objet = $match->name;
        Match::destroy($id);
        return $this->sendResponse($objet, 'Match '.$objet.' a été supprimer.');
    }

    public function search($by,$val){
        if($val == "resume"){
            $val1 = date("2010-01-01 00:00:00");
            $val2 = gmdate("Y-m-d H:i:s");
            $val2 = date("Y-m-d H:i:s",strtotime($val2 . ' - 105 minutes'));
            $matchs = Match::where($by,">",$val1)->where($by,"<",$val2)->orderBy("date","desc")->limit(100)->get();
        }
        else {
            if($val=="now"){
            $val2 = gmdate("Y-m-d H:i:s");
            $val1 = date("Y-m-d H:i:s",strtotime($val2 . ' - 105 minutes'));
            }
            else{
                $val1 = date("Y-m-d H:i:s", strtotime(str_replace("_","-",$val)." 00:00:00"));
                $val2 = date("Y-m-d H:i:s", strtotime(str_replace("_","-",$val)." 23:59:59"));
            }
            $matchs = Match::where($by,">",$val1)->where($by,"<",$val2)->orderBy("date")->limit(100)->get();
        }
        foreach ($matchs as $match) {
            $match->league = League::find($match->league_id);
            $match->home = Team::find($match->home_team);
            $match->away = Team::find($match->away_team);
        }
        return $this->sendResponse($matchs->toArray(), 'Matchs retrieved successfully.');       
    }

    private function genererRef($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return md5($randomString);
    }
}
