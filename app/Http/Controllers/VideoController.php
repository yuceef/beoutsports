<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\APIBaseController as APIBaseController;

class VideoController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::orderBy('id', 'desc')->get();
        return $this->sendResponse($videos->toArray(), 'Videos retrieved successfully.');
    }

    public function pagination($pg=1)
    {
        $videos = Video::orderBy('id', 'desc')->limit($pg*10)->get();
        return $this->sendResponse($videos->toArray(), 'Videos retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required',
            'url' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        
        
        $video = new Video;
        $video->title = $input['title'];
        $video->url = $input['url'];
        $video->description = $input['description'];
        $video->save();
        return $this->sendResponse($video, 'Video '.$video->title.'a été bien ajouter .');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::find($id);
        if (is_null($video))
            return $this->sendError('Video not found.');
        return $this->sendResponse($video->toArray(), 'Video '.$video->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = Video::find($id);
        if (is_null($video))
            return $this->sendError('Video not found.');
        
        $input = $request->all();
        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required',
            'url' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        $video->title = $input['title'];
        $video->url = $input['url'];
        $video->description = $input['description'];
        $video->save();
        return $this->sendResponse($video, 'Video '.$video->title.'a été bien modifier .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::find($id);
        if (is_null($video))
            return $this->sendError('Video not found.');
        $objet = $video->title;
        Video::destroy($id);
        return $this->sendResponse($objet, 'Video '.$objet.' a été supprimer.');
    }
}
