<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;
use App\Http\Controllers\APIBaseController as APIBaseController;
use Validator;

class ChannelController extends APIBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth')->only('store','update','destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $channels = Channel::orderBy('name', 'asc')->get();
        return $this->sendResponse($channels->toArray(), 'Channels retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        $input = $request->all();
        $filename = $_FILES['logo']['name'];
        $validator = Validator::make($input, [
            'name' => 'required|string|unique:channels|max:120',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'code' => 'required|min:5',
            'description' => 'required|string|min:5'
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        $path = $request->file('logo')->storeAs(
            'public/channels', $request->name."_".$request->saison.".".pathinfo($filename, PATHINFO_EXTENSION)
        );
        $channel = new Channel;
        $channel->name = $input['name'];
        $channel->code = $input['code'];
        $channel->ref = $this->genererRef(10);
        $channel->logo = str_replace("public","/storage",$path);
        $channel->description = $input['description'];
        $fp = "./channels/".$channel->ref.".html";
        $handle = fopen($fp, "w+");
        $content = '<html><head><title>'.$channel->name.'</title></head><body>'.$channel->code.'</body></html>';
        fwrite($handle, $content);
        fclose($handle);
        $channel->save();
        return $this->sendResponse($channel, 'Chaîne '.$channel->name.'a été bien ajouter .');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $channel = Channel::find($id);
        if (is_null($channel))
            return $this->sendError('Channel not found.');
        return $this->sendResponse($channel->toArray(), 'Chaîne '.$channel->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $channel = Channel::find($id);
        if (is_null($channel))
            return $this->sendError('Channel not found.');
        $input = $request->all();
        if(isset($_FILES['logo']))$filename = $_FILES['logo']['name'];
        $validator = Validator::make($input, [
            'name' => 'required|string|max:120|unique:channels,id,'.$id,
            'code' => 'required|min:5',
            'description' => 'required|string|min:5'
        ]);
        if($validator->fails()){
            return $this->sendResponse($validator->errors(),'Errors',false);       
        }
        if(isset($_FILES['logo'])){
            $path = $request->file('logo')->storeAs(
                'public/channels', $request->short_name."_".$request->name.".".pathinfo($filename, PATHINFO_EXTENSION)
            );
            $channel->logo = str_replace("public","/storage",$path);
        }
        $channel->name = $input['name'];
        $channel->code = $input['code'];
        //$channel->ref = $this->genererRef(10);
        $fp = "./channels/".$channel->ref.".html";
        $handle = fopen($fp, "w+");
        $content = '<html><head><title>'.$channel->name.'</title></head><body>'.$channel->code.'</body></html>';
        fwrite($handle, $content);
        fclose($handle);
        $channel->description = $input['description'];
        $channel->save();
        return $this->sendResponse($channel, 'Chaîne '.$channel->name.'a été bien modifier .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $channel = Channel::find($id);
        if (is_null($channel))
            return $this->sendError('Channel not found.');
        $objet = $channel->name;
        Channel::destroy($id);
        return $this->sendResponse($objet, 'Chaîne '.$objet.' a été supprimer.');
    }

    private function genererRef($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return md5($randomString);
    }
}
