var app = angular.module('myApp', ['ngRoute'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
app.config(function($routeProvider) {
    $routeProvider.when('/addTeam', {
        templateUrl : '/template/administration/addTeam',
        controller: 'teamController'
    })
    .when('/teams', {
        templateUrl : '/template/administration/teams',
        controller: 'teamController'
    })
    .when('/addLeague', {
        templateUrl : '/template/administration/addLeague',
        controller: 'leagueController'
    })
    .when('/leagues', {
        templateUrl : '/template/administration/leagues',
        controller: 'leagueController'
    })
    .when('/addChannel', {
        templateUrl : '/template/administration/addChannel',
        controller: 'channelController'
    })
    .when('/channels', {
        templateUrl : '/template/administration/channels',
        controller: 'channelController'
    })    
    .when('/addMatch', {
        templateUrl : '/template/administration/addMatch',
        controller: 'matchController'
    })
    .when('/matchs', {
        templateUrl : '/template/administration/matchs',
        controller: 'matchController'
    })
    .when('/addVideo', {
        templateUrl : '/template/administration/addVideo',
        controller: 'videoController'
    })
    .when('/videos', {
        templateUrl : '/template/administration/videos',
        controller: 'videoController'
    })
    .otherwise({
        template : 'hhhh',
    });
});