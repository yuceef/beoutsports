var app = angular.module('myApp', ['ngRoute'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
app.config(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl : '/template/client/calendar',
        controller: 'calendarController'
    })
    .when('/calendar/:date', {
        templateUrl : '/template/client/calendar',
        controller: 'calendarController'
    })
    .when('/matchs/:id/:title', {
        templateUrl : '/template/client/match',
        controller: 'matchController'
    })
    .when('/channels', {
        templateUrl : '/template/client/channels',
        controller: 'channelController'
    })
    .when('/contact', {
        templateUrl : '/template/client/contact'
    })
    .when('/videos', {
        templateUrl : '/template/client/videos',
        controller: 'videosController'
    })    
    .when('/video/:id/:title', {
        templateUrl : '/template/client/video',
        controller: 'videoController'
    })
    .when('/leagues/:id/:name', {
        templateUrl : '/template/client/soon'
    })
    .otherwise({
        templateUrl : '/template/client/soon'
    });
})