app.controller('channelController', function ($scope, $http) {
    $scope.isModel=false
    getChannels()
    $scope.addChannel = function(){
        var fd = new FormData();
        var files = document.getElementById('logo').files[0];
        fd.append('logo',files);
        fd.append('name',$scope.channel.name);
        fd.append('code',$scope.channel.code);
        fd.append('description',$scope.channel.description);
        $http.post("/apiWeb/channels",fd,{headers: {'Content-Type': undefined}}).then(function (res) {
            if (res.data.success) {
                $scope.channel = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.deleteChannel = (id) => {
        $http.delete("/apiWeb/channels/" + id).then(function success(res) {
            if (res.data.success) {
                getChannels()
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else $scope.alert = {iShow: "danger",msg: "Erreur"}
        });
    }
    $scope.upShowChannel = function (id) {
        $http.get("/apiWeb/channels/" + id).then(function (res) {
            if (res.data.success) {
                $scope.channel = res.data.data
                $scope.isModel=true
            }
            else alert(JSON.stringify(res.data.data));
        });
    }
    $scope.upChannel = function (id) {
        $http.patch("/apiWeb/channels/" + id, $scope.channel).then(function (res) {
            if (res.data.success) {
                $scope.channel = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
                getChannels()
                $scope.isModel=false
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.closeModel = function(){
        $scope.isModel=false       
    }
    function getChannels() {
        $http.get('/apiWeb/channels/').then(
            (response) => {
                if (response.data.success) {
                    $scope.channels = response.data.data;
                }
            }
        );
    }
});