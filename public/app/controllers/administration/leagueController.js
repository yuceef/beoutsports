app.controller('leagueController', function ($scope, $http) {
    $scope.isModel=false
    getLeagues()
    $scope.addLeague = function(){
        var fd = new FormData();
        var files = document.getElementById('logo').files[0];
        fd.append('logo',files);
        fd.append('name',$scope.league.name);
        fd.append('saison',$scope.league.saison);
        fd.append('description',$scope.league.description);
        $http.post("/apiWeb/leagues",fd,{headers: {'Content-Type': undefined}}).then(function (res) {
            if (res.data.success) {
                $scope.league = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.deleteLeague = (id) => {
        $http.delete("/apiWeb/leagues/" + id).then(function success(res) {
            if (res.data.success) {
                getLeagues()
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else $scope.alert = {iShow: "danger",msg: "Erreur"}
        });
    }
    $scope.upShowLeague = function (id) {
        $http.get("/apiWeb/leagues/" + id).then(function (res) {
            if (res.data.success) {
                $scope.league = res.data.data
                $scope.isModel=true
            }
            else alert(JSON.stringify(res.data.data));
        });
    }
    $scope.upLeague = function (id) {
        $http.patch("/apiWeb/leagues/" + id, $scope.league).then(function (res) {
            if (res.data.success) {
                $scope.league = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
                getLeagues()
                $scope.isModel=false
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.closeModel = function(){
        $scope.isModel=false       
    }
    function getLeagues() {
        $http.get('/apiWeb/leagues/').then(
            (response) => {
                if (response.data.success) {
                    $scope.leagues = response.data.data;
                }
            }
        );
    }
});