app.controller('videoController', function ($scope, $http) {
    $scope.isModel=false
    getVideos()
    $scope.addVideo = function(){
        $http.post("/apiWeb/videos",$scope.video).then(function(res){
            if (res.data.success) {
                $scope.video = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.deleteVideo = (id) => {
        $http.delete("/apiWeb/videos/" + id).then(function success(res) {
            if (res.data.success) {
                getVideos()
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else $scope.alert = {iShow: "danger",msg: "Erreur"}
        });
    }
    $scope.upShowVideo = function (id) {
        $http.get("/apiWeb/videos/" + id).then(function (res) {
            if (res.data.success) {
                $scope.video = res.data.data
                $scope.isModel=true
            }
            else alert(JSON.stringify(res.data.data));
        });
    }
    $scope.upVideo = function (id) {
        $http.patch("/apiWeb/videos/" + id, $scope.video).then(function (res) {
            if (res.data.success) {
                $scope.video = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
                getVideos()
                $scope.isModel=false
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.closeModel = function(){
        $scope.isModel=false       
    }
    $scope.generer = function(){

        $http.get("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id="+$scope.getCode($scope.video.url)+"&key=AIzaSyCSJ8jxL9lB_GjCBGe_kh1Yu40eUno2dbE")
        .then(function(res){
            $scope.video.title = res.data.items[0].snippet.title
            $scope.video.description = res.data.items[0].snippet.description
        })
    }
    $scope.getCode = function(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }
    function getVideos() {
        $http.get('/apiWeb/videos/').then(
            (response) => {
                if (response.data.success) {
                    $scope.videos = response.data.data;
                }
            }
        );
    }
})