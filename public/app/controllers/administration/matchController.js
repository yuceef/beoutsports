app.controller('matchController', function ($scope, $http) {
    $scope.isModel=false
    getMatchs(new Date())
    getTeams()
    getLeagues()
    getChannels()
    $scope.getMatchs = function(url){
        getMatchs(url)
    }
    $scope.addMatch = function(){
        var todayTime = $scope.dateM;
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
        var toTime = $scope.timeM;
        var heure = toTime.getHours()
        var min = toTime.getMinutes()
        $scope.match.date = year + "-" + month + "-" + day + " "+heure+":"+min+":00";
        $http.post("/apiWeb/matchs",$scope.match).then(function (res) {
            if (res.data.success) {
                $scope.match = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.deleteMatch = (id) => {
        $http.delete("/apiWeb/matchs/" + id).then(function success(res) {
            if (res.data.success) {
                getMatchs()
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else $scope.alert = {iShow: "danger",msg: "Erreur"}
        });
    }
    $scope.upShowMatch = function (id) {
        $http.get("/apiWeb/matchs/" + id).then(function (res) {
            if (res.data.success) {
                $scope.match = res.data.data
                $scope.match.channels=$scope.match.channels.split(",")
                $scope.isModel=true
            }
            else alert(JSON.stringify(res.data.data));
        });
    }
    $scope.upMatch = function (id) {
        if($scope.match.dateM && $scope.match.timeM){
            var todayTime = $scope.match.dateM;
            var month = todayTime.getMonth() + 1;
            var day = todayTime.getDate();
            var year = todayTime.getFullYear();
            var toTime = $scope.match.timeM;
            var heure = toTime.getHours()
            var min = toTime.getMinutes()
            $scope.match.date = year + "-" + month + "-" + day + " "+heure+":"+min+":00";
        }
        $http.patch("/apiWeb/matchs/" + id, $scope.match).then(function (res) {
            if (res.data.success) {
                $scope.match = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
                getMatchs()
                $scope.isModel=false
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.closeModel = function(){
        $scope.isModel=false       
    }
    $scope.isNovideo = function(mat){
        var d1 = new Date();
        var d2 = new Date(mat.date);

        return !mat.video && d1.getTime() >= 105*60000+d2.getTime()
    }
    $scope.getLeague = function(id){
        if($scope.leagues){
            for(var i=0;i<$scope.leagues.length;i++){
                if($scope.leagues[i].id==id)return $scope.leagues[i].logo
            }
        }
        return "";       
    }
    $scope.getTeam = function(id){
        if($scope.teams){
            for(var i=0;i<$scope.teams.length;i++){
            if($scope.teams[i].id==id)return $scope.teams[i].logo
            }
        }
        return "";  
    }
    function getMatchs(url) {
        now = new Date(url)
        val = now.getFullYear()+"_"+(now.getMonth()+1)+"_"+now.getDate()
        $http.get('/apiWeb/matchs/date/'+val).then(
            (response) => {
                if (response.data.success) {
                    $scope.matchs = response.data.data;
                }
            }
        );
    }
    function getTeams() {
        $http.get('/apiWeb/teams/').then(
            (response) => {
                if (response.data.success) {
                    $scope.teams = response.data.data;
                }
            }
        );
    }
    function getLeagues() {
        $http.get('/apiWeb/leagues/').then(
            (response) => {
                if (response.data.success) {
                    $scope.leagues = response.data.data;
                }
            }
        );
    }
    function getChannels() {
        $http.get('/apiWeb/channels/').then(
            (response) => {
                if (response.data.success) {
                    $scope.channels = response.data.data;
                }
            }
        );
    }
});