app.controller('teamController', function ($scope, $http) {
    $scope.isModel=false
    getTeams()
    $scope.addTeam = function(){
        var fd = new FormData();
        var files = document.getElementById('logo').files[0];
        fd.append('logo',files);
        fd.append('name',$scope.team.name);
        fd.append('short_name',$scope.team.short_name);
        fd.append('nationality',$scope.team.nationality);
        fd.append('description',$scope.team.description);
        $http.post("/apiWeb/teams",fd,{headers: {'Content-Type': undefined}}).then(function (res) {
            if (res.data.success) {
                $scope.team = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.deleteTeam = (id) => {
        $http.delete("/apiWeb/teams/" + id).then(function success(res) {
            if (res.data.success) {
                getTeams()
                $scope.alert = {iShow: "success",msg: res.data.message}
            }
            else $scope.alert = {iShow: "danger",msg: "Erreur"}
        });
    }
    $scope.upShowTeam = function (id) {
        $http.get("/apiWeb/teams/" + id).then(function (res) {
            if (res.data.success) {
                $scope.team = res.data.data
                $scope.isModel=true
            }
            else alert(JSON.stringify(res.data.data));
        });
    }
    $scope.upTeam = function (id) {
        $http.patch("/apiWeb/teams/" + id, $scope.team).then(function (res) {
            if (res.data.success) {
                $scope.team = {}
                $scope.alert = {iShow: "success",msg: res.data.message}
                getTeams()
                $scope.isModel=false
            }
            else {
                jsonObj = res.data.data
                msg = ""
                for(var key in jsonObj) {
                    if(jsonObj.hasOwnProperty(key)) {
                        msg += jsonObj[key][0];
                        break;
                    }
                }
                $scope.alert = {iShow: "danger",msg: msg}
            }
        });
    }
    $scope.closeModel = function(){
        $scope.isModel=false       
    }
    function getTeams() {
        $http.get('/apiWeb/teams/').then(
            (response) => {
                if (response.data.success) {
                    $scope.teams = response.data.data;
                }
            }
        );
    }
});