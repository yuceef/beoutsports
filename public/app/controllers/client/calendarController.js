app.controller('calendarController', function ($scope, $rootScope, $routeParams, $http, $location) {
    $rootScope.titre = "مباريات "
    getMatchs()
    getLeagues()
    $scope.timeMatch = function(dt){
        d = new Date(dt)
        var hours = d.getHours();
        var min = d.getMinutes();
        if((min + '').length == 1){
            min = '0' + min;
        }
        return hours+":"+min
    }
    $scope.direction = function(url,isdate=false){
        if(isdate){
            now = new Date(url)
            url = 'calendar/'+now.getFullYear()+"_"+(now.getMonth()+1)+"_"+now.getDate()
        }
        $location.path(url)
    }
    $scope.isLive = function(dt){
        var d1 = new Date();
        d1 = d1.toGMTString();
        var d2 = new Date(dt);
        if(d1.getTime() >= d2.getTime() && d1.getTime() <= 110*60000+d2.getTime()){
            return "live"
        }
        else if(d1.getTime() < d2.getTime()){
            return "avant"
        }
        else return "apres"
    }
    $scope.timeOfMatch = function(dt){
        var d1 = new Date();
        d1 = d1.toGMTString();
        var d2 = new Date(dt);
        diff = Math.ceil((d1.getTime()-d2.getTime())/60000)
        if(diff<=45) return diff
        else if(45<diff && diff<60) return "Time"
        else return diff-15
    }
    function getMatchs(){
        if($routeParams.date == "today" || !$routeParams.date){
            now = new Date()
            now = now.toGMTString();
            val = now.getFullYear()+"_"+(now.getMonth()+1)+"_"+now.getDate()
            $scope.titre = "اليوم"
        }
        else if($routeParams.date == "now"){
            val = $routeParams.date
            $scope.titre = "الآن"
        }
        else if($routeParams.date == "resume"){
            val = $routeParams.date
            $scope.titre = "وملخصات"
        }
        else if($routeParams.date == "yesterday"){
            now = new Date()
            now = now.toGMTString();
            val = now.getFullYear()+"_"+(now.getMonth()+1)+"_"+(now.getDate() + "-1 days")
            $scope.titre = "الامس"
        }
        else if($routeParams.date == "tomorrow"){
            now = new Date()
            now = now.toGMTString();
            val = now.getFullYear()+"_"+(now.getMonth()+1)+"_"+(now.getDate() + "1 days")
            $scope.titre = "الغد"
        }
        else{
            val = $routeParams.date
            $scope.titre = val.split("_").join("/")
        }
        $rootScope.titre = $rootScope.titre + $scope.titre
        $http.get('/apiWeb/matchs/date/'+val).then(
            (response) => {
                if (response.data.success) {
                    $scope.matchs = response.data.data;
                }
            }
        );
    }
    function getLeagues() {
        $http.get('/apiWeb/leagues/').then(
            (response) => {
                if (response.data.success) {
                    $scope.leagues = response.data.data;
                }
            }
        );
    }        
})