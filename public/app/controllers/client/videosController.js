app.controller('videosController', function ($scope, $rootScope, $routeParams, $http, $location) {
    $scope.getCode = function(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }
    $scope.getVideos = (pg) => {
        $scope.pg = pg + 1
        $http.get('/apiWeb/videos/page/'+pg).then(
            (response) => {
                if (response.data.success) {
                    $scope.videos = response.data.data;
                }
            }
        );
    }  
    $scope.getVideos(1)

})