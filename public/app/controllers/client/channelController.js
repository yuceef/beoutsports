app.controller('channelController', function ($scope, $rootScope, $http) {
    $rootScope.titre = "القنوات"
    getChannels()
    function getChannels(){
        $http.get('/apiWeb/channels/').then(
            (response) => {
                if (response.data.success) {
                    $scope.channels = response.data.data;
                }
            }
        );
    }
})