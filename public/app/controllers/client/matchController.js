app.controller('matchController', function ($scope, $rootScope, $routeParams, $http, $location) {
    $rootScope.titre = "مباراة "
    getMatch()
    $scope.changeCode = function(ref){
        $scope.liveCode ='<iframe width="100%" height="500" src="/channels/'+ref+'.html" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
        $scope.myRef = ref
    }
    $scope.getTimeLive = function(dt){
        var d1 = new Date();
        d1 = d1.toGMTString();
        var d2 = new Date(dt);
        if(d1.getTime() >= d2.getTime() && d1.getTime() <= 105*60000+d2.getTime()){
            diff = Math.ceil((d1.getTime()-d2.getTime())/60000)
            if(diff<=45) return diff
            else if(45<diff && diff<60) return 45
            else return diff-15
        }
        else if(d1.getTime() < d2.getTime()){
            return "لم تبدأ"
        }
        else return "انتهت"
    }
    $scope.isLive = function(dt){
        var d1 = new Date();
        d1 = d1.toGMTString();
        var d2 = new Date(dt);
        if(d1.getTime() >= d2.getTime() && d1.getTime() <= 110*60000+d2.getTime()){
            return true
        }
        else return false
    }
    function getMatch(){
        $http.get('/apiWeb/matchs/'+$routeParams.id).then(
            (response) => {
                if (response.data.success) {
                    $scope.match = response.data.data;
                    $scope.myRef = response.data.data.infoChannel[0].ref;
                    $scope.liveCode ='<iframe width="100%" height="580"  src="/channels/'+response.data.data.infoChannel[0].ref+'.html" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
                    $scope.videoCode ='<iframe width="100%" height="500" src="'+response.data.data.video+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
                    $rootScope.titre = $rootScope.titre + ":" + response.data.data.home.name+" Vs "+ response.data.data.away.name
                }
            }
        );
    }
})
.filter('renderHTMLCorrectly', function($sce)
{
	return function(stringToParse)
	{
		return $sce.trustAsHtml(stringToParse);
	}
});