app.controller('videoController', function ($scope, $rootScope, $routeParams, $http, $location) {
    $rootScope.titre = ""
    getVideo()
    $scope.getCode = function(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }
    function getVideo(){
        $http.get('/apiWeb/videos/'+$routeParams.id).then(
            (response) => {
                if (response.data.success) {
                    $scope.video = response.data.data;
                    $scope.video.code ='<iframe width="100%" height="500" src="https://www.youtube.com/embed/'+$scope.getCode(response.data.data.url)+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
                    $rootScope.titre = response.data.data.title
                }
            }
        );
    }

})
.filter('renderHTMLCorrectly', function($sce)
{
	return function(stringToParse)
	{
		return $sce.trustAsHtml(stringToParse);
	}
});